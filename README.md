# welcome 

Yii Rest api dengan **credentials** -> **Basic Auth**

[http://localhost/blogapi/web/api/api-post](http://localhost/blogapi/web/api/api-post)

[http://localhost/blogapi/web/api/api-account](http://localhost/blogapi/web/api/api-account)

## screenshot
### Username dan password salah

![enter image description here](https://gitlab.com/kadarisman/yii-blog-api/raw/master/ss/get%20index%20F.PNG)

### Username dan password benar

![enter image description here](https://gitlab.com/kadarisman/yii-blog-api/raw/master/ss/get%20index%20T.PNG)

### Create
![enter image description here](https://gitlab.com/kadarisman/yii-blog-api/raw/master/ss/create.PNG)

### Cek Create
![enter image description here](https://gitlab.com/kadarisman/yii-blog-api/raw/master/ss/create%20cek.PNG)

### View

![enter image description here](https://gitlab.com/kadarisman/yii-blog-api/raw/master/ss/view.PNG)


### Update
![enter image description here](https://gitlab.com/kadarisman/yii-blog-api/raw/master/ss/update.PNG)

### Delete
![enter image description here](https://gitlab.com/kadarisman/yii-blog-api/raw/master/ss/delete.PNG)


### Cek Delete
![enter image description here](https://gitlab.com/kadarisman/yii-blog-api/raw/master/ss/cek%20delete.PNG)
