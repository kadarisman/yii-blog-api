<?php

namespace app\modules\api\controllers;
use app\models\Post;
use app\models\User;


use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;

class SummaryController extends \yii\web\Controller
{

    public $modelClass = 'app\models\Post';

    public $enableCsrfValidation = false;


    public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
			'class' => HttpBasicAuth::className(),
			'auth' => function ($username, $password) {
                $user = user::findByUsername($username);
                
				if ($user && $user->validatePassword($password, $password)) {
					return $user;
				}

			}

		];

		return $behaviors;

	}






    public function actionCreateSummary()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $summary = new Post();
        $summary->scenario = Post::SCENARIO_CREATE;
        $summary->attributes = \Yii::$app->request->post();
        // return array('status' => true);

        if($summary->validate()){
            $summary->save();
            return array('status' => true, 'data' => 'Summary created successfully.');
        }else{
            return array('status' => false, 'data' => $summary->getErrors());
        }
    }

    public function actionListSummary()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $summary = Post::find()->all();
        if(count($summary) > 0){
            return array('status' => true, 'data' => $summary);
        }else{
            return array('status' => false, 'data' => 'No data summary.');
        }
    }

    public function actionIndex()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $summary = Post::find()->all();
        if(count($summary) > 0){
            return array('status' => true, 'data' => $summary);
        }else{
            return array('status' => false, 'data' => 'No data summary.');
        }
    }

}
